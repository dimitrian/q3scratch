#
# SECTION: Configuration variables
#
# Every Makefile should contain this line:
SHELL = /bin/sh

# Quiet output do not show full build commands
Q := @

# Build mode: debug or release
BUILD_MODE ?= debug
# Build output top directory. Make will create another dir for actual build mode.
BUILD_DIR ?= ./build

INSTALL_DIR ?= ~/.q3a/baseq3
INSTALL_PAK ?= pak8z.pk3 

# Tools: gcc, q3asm, q3lcc
CC := gcc
ASM := q3asm 
# TODO: looks like q3lcc can't find q3cpp if path is not specified. By calling which q3lcc called used full path.
# see etc/bytecode.c:updatePaths
LCC := $(shell which q3lcc)

# Experimental usage of Tiny C Compiler
USE_TCC ?= 0

# Source files dirs:
G_SRC_DIR := ./code/game
CG_SRC_DIR := ./code/cgame

#
# SECTION: Define compile platform 
#
COMPILE_PLATFORM=$(shell uname | sed -e 's/_.*//' | tr '[:upper:]' '[:lower:]' | sed -e 's/\//_/g')

ifeq ($(COMPILE_PLATFORM),mingw32)
  ifeq ($(COMPILE_ARCH),i386)
    COMPILE_ARCH=x86
  endif
endif

ifeq ($(COMPILE_PLATFORM),cygwin)
  COMPILE_PLATFORM=mingw32
endif

PLATFORM := $(COMPILE_PLATFORM)

#
# SECTION: Define compile architecture
#
COMPILE_ARCH=$(shell uname -m | sed -e 's/i.86/x86/' | sed -e 's/^arm.*/arm/')

ifeq ($(COMPILE_ARCH),i86pc)
  COMPILE_ARCH=x86
endif

ifeq ($(COMPILE_ARCH),amd64)
  COMPILE_ARCH=x86_64
endif

ifeq ($(COMPILE_ARCH),x64)
  COMPILE_ARCH=x86_64
endif

ARCH := $(COMPILE_ARCH)

#
# SECTION: Define platform specific arguments
#
# Base compiler flags: CFLAGS
# Those flags are passed to linked when creating a shared library and to compiler to produce *.c => *.o
# https://man7.org/linux/man-pages/man1/gcc.1.html
#  -Wall                                  Turns on most, but not all, compiler warnings 
#  -Wdisabled-optimization                Requested optimization pass disabled e.g. code is too big or complex
#  -Werror-implicit-function-declaration  Whenever a function is used before being declared.
#  -Wformat-security                      Format string is not a string literal and there are no format arguments as in "printf(foo)" 
#  -Wformat=2                             Equivalent to -Wformat -Wformat-nonliteral -Wformat-security -Wformat-y2k
#  -Wimplicit
#  -Wmissing-format-attribute
#  -Wno-format-nonliteral
#  -Wno-format-zero-length 
#  -Wstrict-aliasing=2
#  -Wstrict-prototypes
#  -fno-strict-aliasing                   Warn when compiler cannot assume the value of data when two pointers of different types are accessing the same location
#  -pipe                                  Use pipes rather than temporary files for communication between the various stages of compilation.
CFLAGS := -Wall -fno-strict-aliasing -Wimplicit -Wstrict-prototypes -pipe
CFLAGS += -Wformat=2 -Wno-format-zero-length -Wformat-security -Wno-format-nonliteral
CFLAGS += -Wstrict-aliasing=2 -Wmissing-format-attribute
CFLAGS += -Wdisabled-optimization
CFLAGS += -Werror-implicit-function-declaration
# -std=c90 == -ansi
# CFLAGS += -ansi
# CFLAGS += -std=c99
# The particular standard is used by -pedantic to identify which features 
# are GNU extensions given that version of the standard. 
# For example `-std=gnu89 -pedantic' would warn about C++ style `//' comments, 
# while `-std=gnu99 -pedantic' would not.
# CFLAGS += -std=gnu89 -pedantic
CFLAGS += -std=gnu89

ifeq ($(ARCH),x86)
  #  The -m32 option sets "int", "long", and pointer types to 32 bits, and generates code that runs on any i386 system.
  CFLAGS += -m32
endif

ifeq ("$(CC)", $(findstring "$(CC)", "clang" "clang++"))
  CFLAGS += -Qunused-arguments
endif

# Experimental usage of Tiny C Compiler
ifeq ($(USE_TCC),1)
  CC := ~/tcc/tcc-0.9.27/tcc
  CFLAGS += -B/home/zus/tcc/tcc-0.9.27 -I/home/zus/tcc/tcc-0.9.27/include
  # TODO: how to remove option? Move this section down?
  # LIB_CFLAGS := $(filter-out -MMD, $(LIB_CFLAGS))
  ifeq ($(BUILD_MODE),debug)
    # Seems like TCC needs -ggdb flag for compiler, not only for linker
    CFLAGS += -ggdb
  endif
endif

#  Optimization options: -f*
#  Used with compiler target *.c => *.o
#  -fvisibility=hidden   Can very substantially improve linking and load times of shared object libraries, 
#                        produce more optimized code, provide near-perfect API export and prevent symbol clashe
#  -fomit-frame-pointer  Don't waste a frame pointer register. Enabled by default at -O and higher.
#  -ffast-math           Multiple math optimizations. This option causes the preprocessor macro "__FAST_MATH__" to be defined.
#                        Most of the “fast math” optimizations can be enabled/disabled individually, 
#                        and -ffast-math enables all of them:
#                         -ffinite-math-only
#                         -fno-signed-zeros
#                         -fno-trapping-math
#                         -fassociative-math
#                         -fno-math-errno
#                         -freciprocal-math
#                         -funsafe-math-optimizations
#                         -fcx-limited-range
#  -DNDEBUG              NDEBUG is used in source files to strip debug messages
OPTIMIZE := -DNDEBUG -O2 -fvisibility=hidden -fomit-frame-pointer -ffast-math
ifeq ($(BUILD_MODE),debug)
  # -ggdb                Produce debugging information for use by GDB 
  # TODO: Do I need both -g + -ggdb?
  OPTIMIZE := -ggdb -g -O0
endif

ifeq ($(PLATFORM),linux)
  LIB_EXT := .so
  # -fPIC  If supported for the target machine, emit position-
  #        independent code, suitable for dynamic linking and avoiding
  #        any limit on the size of the global offset table.  This
  #        option makes a difference on AArch64, m68k, PowerPC and SPARC.
  LIB_CFLAGS :=-fPIC
  LIB_LDFLAGS=-shared
endif # linux

# Activate *.d creation by setting -MMD rebuild when *.h files change
# https://codereview.stackexchange.com/questions/2547/makefile-dependency-generation/11109#11109
# Create dependency files *.d files from *.c
# A *.d file is a source dependency file generated by GCC, a GNU C compiler. 
# It contains dependencies in plain text that describe the files that were used to create compiled objects (.o files) by a C compiler. 
LIB_CFLAGS += -MMD

#
# SECTION: Define output directories, lib filenames, sources, obj files
#
BUILD_DIR := $(BUILD_DIR)/$(BUILD_MODE)-$(PLATFORM)-$(ARCH)

# 
# SECTION: Game input and output files
#
G_OBJ_DIR := $(BUILD_DIR)/game
G_LIB := $(BUILD_DIR)/qagame$(ARCH)$(LIB_EXT)
G_QVM := $(BUILD_DIR)/vm/qagame.qvm
G_SRCS := $(wildcard $(G_SRC_DIR)/*.c)
G_OBJS := $(patsubst $(G_SRC_DIR)/%.c, $(G_OBJ_DIR)/%.o, $(G_SRCS))

G_ASMS := $(patsubst $(G_SRC_DIR)/%.c, $(G_OBJ_DIR)/%.asm, $(G_SRCS))
# g_syscalls.asm is already exist and needs to be copied as-is, must not be compiled from g_syscalls.c
G_ASMS := $(filter-out %g_syscalls.asm, $(G_ASMS))

# g_main.c with vmMain must be the first file for QVM
G_ASM_SORTED_LIST := $(G_OBJ_DIR)/g_main.asm $(G_OBJ_DIR)/g_syscalls.asm $(filter-out %g_main.asm, $(G_ASMS))

#
# SECTION: CGame input and output files
# 
CG_OBJ_DIR := $(BUILD_DIR)/cgame
CG_LIB := $(BUILD_DIR)/cgame$(ARCH)$(LIB_EXT)
CG_QVM := $(BUILD_DIR)/vm/cgame.qvm
CG_SRCS := $(wildcard $(CG_SRC_DIR)/*.c)

# Both Games objects code/game/bg_%.c => build/**/cgame/bg_%.o and q_%.c => q_%.o
BG_OBJS := $(wildcard $(G_SRC_DIR)/bg_*.c)
BG_OBJS += $(wildcard $(G_SRC_DIR)/q_*.c)
BG_OBJS := $(patsubst $(G_SRC_DIR)/%.c, $(CG_OBJ_DIR)/%.o, $(BG_OBJS))

CG_OBJS := $(patsubst $(CG_SRC_DIR)/%.c, $(CG_OBJ_DIR)/%.o, $(CG_SRCS))
CG_OBJS += $(BG_OBJS)

# Add all source code/cgame/%.c => build/**/cgame/%.asm
CG_ASMS := $(patsubst $(CG_SRC_DIR)/%.c, $(CG_OBJ_DIR)/%.asm, $(CG_SRCS))
# Add Both Games objects build/**/cgame/bg_%.o => build/**/cgame/bg_%.asm
CG_ASMS += $(patsubst $(CG_OBJ_DIR)/%.o, $(CG_OBJ_DIR)/%.asm, $(BG_OBJS))
# g_syscalls.asm is already exist and needs to be copied as-is, must not be compiled from g_syscalls.c
CG_ASMS := $(filter-out %cg_syscalls.asm, $(CG_ASMS))

# cg_main.c with vmMain must be the first file for QVM
CG_ASM_SORTED_LIST := $(CG_OBJ_DIR)/cg_main.asm $(CG_OBJ_DIR)/cg_syscalls.asm $(filter-out %cg_main.asm, $(CG_ASMS))

#
# SECTION: Build rules and targets 
# 
# Default target
# By default, the Make rules should compile and link with ‘-g’, so
# that executable programs have debugging symbols. 
# .NOTPARALLEL:
.PHONY: all
all: dirs libs qvms test

.PHONY: print-info
print-info:
	@echo "Platform $(COMPILE_PLATFORM) $(COMPILE_ARCH)"
	@echo "Build in $(BUILD_DIR)"
	@echo "Install in $(INSTALL_DIR)/$(INSTALL_PAK)"

.PHONY: dirs
dirs:
	@if [ ! -d $(G_OBJ_DIR) ];then mkdir --parents --verbose $(G_OBJ_DIR);fi
	@if [ ! -d $(CG_OBJ_DIR) ]; then mkdir --parents --verbose $(CG_OBJ_DIR);fi
	@if [ ! -d $(BUILD_DIR)/vm ]; then mkdir --parents --verbose $(BUILD_DIR)/vm;fi

.PHONY: install
install: install-libs install-qvms

.PHONY: uninstall
uninstall: uninstall-libs uninstall-qvms

.PHONY: install-libs
install-libs: dirs libs
	cp $(G_LIB) $(INSTALL_DIR)
	cp $(CG_LIB) $(INSTALL_DIR)

.PHONY: uninstall-libs
uninstall-libs:
	rm -f $(INSTALL_DIR)/*.so

.PHONY: clean
clean:
	rm -rf $(BUILD_DIR)/*

.PHONY: install-qvms
install-qvms: dirs qvms
	cd $(BUILD_DIR) && zip -r $(INSTALL_PAK) vm && cp $(INSTALL_PAK) $(INSTALL_DIR)

.PHONY: uninstall-qvms
uninstall-qvms:
	rm -f $(INSTALL_DIR)/$(INSTALL_PAK)	

.PHONY: docs
docs:
	doxygen Doxyfile

.PHONY: libs
libs: dirs $(G_LIB) $(CG_LIB)

.PHONY: qvms
qvms: dirs $(G_QVM) $(CG_QVM)

#
# SECTION: Game shared library targets
#
$(G_LIB): $(G_OBJS)
	@echo "G_LD $@"
	$(Q)$(CC) $(CFLAGS) $(LIB_LDFLAGS) -o $@ $(G_OBJS)

$(G_OBJ_DIR)/%.o: $(G_SRC_DIR)/%.c
	@echo "G_CC $< => $@"
	$(Q)$(CC) -DQAGAME $(CFLAGS) $(LIB_CFLAGS) $(OPTIMIZE) -o $@ -c $<

#
# SECTION: Game QVM targets
#
$(G_QVM): $(G_ASMS)
	@echo "G_ASM: $@"
	$(Q)cp $(G_SRC_DIR)/g_syscalls.asm $(G_OBJ_DIR)
	@# its important to set -vq3 flag for new q3asm 
	@# or qvm's will not run on original 1.32c binaries
	@# see also:
	@#    -f LISTFILE *.q3asm
	@#    -v verbose output        
	$(Q)$(ASM) -vq3 -r -m -o $@ $(G_ASM_SORTED_LIST)

$(G_OBJ_DIR)/%.asm: $(G_SRC_DIR)/%.c
	@echo "G_LCC $< => $@"
	$(Q)$(LCC) -DQ3_VM -DQAGAME -S -Wf-g -I$(G_SRC_DIR) -o $@ $<

#
# SECTION: CGame shared library targets
#
$(CG_LIB): $(CG_OBJS)
	@echo "CG_LD $@"
	$(Q)$(CC) $(CFLAGS) $(LIB_LDFLAGS) -o $@ $(CG_OBJS)

.PHONY: test
test: $(BUILD_DIR)/test_cgame $(BUILD_DIR)/test_game

# Catch liker errors for cgame
$(BUILD_DIR)/test_cgame: $(CG_LIB)
	@echo "TEST $@"
	$(Q)$(CC) $(CFLAGS) -Icode/cgame -o $@ code/test/test_cgame.c $(CG_OBJS) -lm
	$(Q)./$@

# Catch linker errors for game
$(BUILD_DIR)/test_game: $(G_LIB)
	@echo "TEST $@"
	$(Q)$(CC) $(CFLAGS) -Icode/game -o $@ code/test/test_game.c $(G_OBJS) -lm
	$(Q)./$@

$(CG_OBJ_DIR)/bg_%.o: $(G_SRC_DIR)/bg_%.c
	@echo "CG_CC $< => $@"
	$(Q)$(CC) -DCGAME $(CFLAGS) $(LIB_CFLAGS) $(OPTIMIZE) -o $@ -c $<

$(CG_OBJ_DIR)/q_%.o: $(G_SRC_DIR)/q_%.c
	@echo "CG_CC $< => $@"
	$(Q)$(CC) -DCGAME $(CFLAGS) $(LIB_CFLAGS) $(OPTIMIZE) -o $@ -c $<

$(CG_OBJ_DIR)/%.o: $(CG_SRC_DIR)/%.c
	@echo "CG_CC $< => $@"
	$(Q)$(CC) -DCGAME $(CFLAGS) $(LIB_CFLAGS) $(OPTIMIZE) -o $@ -c $<

#
# SECTION: CGame QVM targets
#
$(CG_QVM): $(CG_ASMS)
	@echo "CG_ASM: $@"
	$(Q)cp $(CG_SRC_DIR)/cg_syscalls.asm $(CG_OBJ_DIR)
	@# its important to set -vq3 flag for new q3asm 
	@# or qvm's will not run on original 1.32c binaries
	@# see also: 
	@#    -f LISTFILE *.q3asm
	@#    -v verbose output        
	$(Q)$(ASM) -vq3 -r -m -o $@ $(CG_ASM_SORTED_LIST)

$(CG_OBJ_DIR)/bg_%.asm: $(G_SRC_DIR)/bg_%.c
	@echo "CG_LCC $< => $@"
	$(Q)$(LCC) -DQ3_VM -DCGAME -S -Wf-g -I$(CG_SRC_DIR) -o $@ $<

$(CG_OBJ_DIR)/q_%.asm: $(G_SRC_DIR)/q_%.c
	@echo "CG_LCC $< => $@"
	$(Q)$(LCC) -DQ3_VM -DCGAME -S -Wf-g -I$(CG_SRC_DIR) -o $@ $<

$(CG_OBJ_DIR)/%.asm: $(CG_SRC_DIR)/%.c
	@echo "CG_LCC $< => $@"
	$(Q)$(LCC) -DQ3_VM -DCGAME -S -Wf-g -I$(CG_SRC_DIR) -o $@ $<

#
# SECTION: Include dependency files
#
D_FILES=$(shell find . -name '*.d')
ifneq ($(strip $(D_FILES)),)
  include $(D_FILES)
endif
