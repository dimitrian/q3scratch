/// @file cg_public.h
/// Client Game module information visible to client system.
///
/// Server includes and uses information defined in this file!

#define MAX_ENTITIES_IN_SNAPSHOT 256

/// Snapshots are a view of the server at a given time.
/// Snapshots are generated at regular time intervals by the server,
/// but they may not be sent if a client's rate level is exceeded, or
/// they may be dropped by the network.
/// @ingroup _sent_across_net
typedef struct {
	/// #SNAPFLAG_RATE_DELAYED, etc
	int snapFlags;
	int ping;

	/// Server time the message is valid for (in msec).
	int serverTime;

	/// Portalarea visibility bits.
	byte areamask[MAX_MAP_AREA_BYTES];

	/// Complete information about the current player at this time.
	playerState_t ps;

	/// All of the entities that need to be presented.
	int numEntities;
	/// At the time of this snapshot.
	entityState_t entities[MAX_ENTITIES_IN_SNAPSHOT];

	/// Text based server commands to execute when this
	/// snapshot becomes current.
	int numServerCommands;
	int serverCommandSequence;
} snapshot_t;

/// Functions imported from the main executable.
typedef enum {
	CG_PRINT,
	CG_ERROR,
	CG_MILLISECONDS,
	CG_CVAR_REGISTER,
	CG_CVAR_UPDATE,
	CG_CVAR_SET,
	CG_CVAR_VARIABLESTRINGBUFFER,
	CG_ARGC,
	CG_ARGV,
	CG_ARGS,
	CG_FS_FOPENFILE,
	CG_FS_READ,
	CG_FS_WRITE,
	CG_FS_FCLOSEFILE,
	CG_SENDCONSOLECOMMAND,
	CG_ADDCOMMAND,
	CG_SENDCLIENTCOMMAND,
	CG_UPDATESCREEN,
	CG_CM_LOADMAP,
	CG_CM_NUMINLINEMODELS,
	CG_CM_INLINEMODEL,
	CG_CM_LOADMODEL,
	CG_CM_TEMPBOXMODEL,
	CG_CM_POINTCONTENTS,
	CG_CM_TRANSFORMEDPOINTCONTENTS,
	CG_CM_BOXTRACE,
	CG_CM_TRANSFORMEDBOXTRACE,
	CG_CM_MARKFRAGMENTS,
	CG_S_STARTSOUND,
	CG_S_STARTLOCALSOUND,
	CG_S_CLEARLOOPINGSOUNDS,
	CG_S_ADDLOOPINGSOUND,
	CG_S_UPDATEENTITYPOSITION,
	CG_S_RESPATIALIZE,
	CG_S_REGISTERSOUND,
	CG_S_STARTBACKGROUNDTRACK,
	CG_R_LOADWORLDMAP,
	CG_R_REGISTERMODEL,
	CG_R_REGISTERSKIN,
	CG_R_REGISTERSHADER,
	CG_R_CLEARSCENE,
	CG_R_ADDREFENTITYTOSCENE,
	CG_R_ADDPOLYTOSCENE,
	CG_R_ADDLIGHTTOSCENE,
	CG_R_RENDERSCENE,
	CG_R_SETCOLOR,
	CG_R_DRAWSTRETCHPIC,
	CG_R_MODELBOUNDS,
	CG_R_LERPTAG,
	CG_GETGLCONFIG,
	CG_GETGAMESTATE,
	CG_GETCURRENTSNAPSHOTNUMBER,
	CG_GETSNAPSHOT,
	CG_GETSERVERCOMMAND,
	CG_GETCURRENTCMDNUMBER,
	CG_GETUSERCMD,
	CG_SETUSERCMDVALUE,
	CG_R_REGISTERSHADERNOMIP,
	CG_MEMORY_REMAINING,
	CG_R_REGISTERFONT,
	CG_KEY_ISDOWN,
	CG_KEY_GETCATCHER,
	CG_KEY_SETCATCHER,
	CG_KEY_GETKEY,
	CG_PC_ADD_GLOBAL_DEFINE,
	CG_PC_LOAD_SOURCE,
	CG_PC_FREE_SOURCE,
	CG_PC_READ_TOKEN,
	CG_PC_SOURCE_FILE_AND_LINE,
	CG_S_STOPBACKGROUNDTRACK,
	CG_REAL_TIME,
	CG_SNAPVECTOR,
	CG_REMOVECOMMAND,
	CG_R_LIGHTFORPOINT,
	CG_CIN_PLAYCINEMATIC,
	CG_CIN_STOPCINEMATIC,
	CG_CIN_RUNCINEMATIC,
	CG_CIN_DRAWCINEMATIC,
	CG_CIN_SETEXTENTS,
	CG_R_REMAP_SHADER,
	CG_S_ADDREALLOOPINGSOUND,
	CG_S_STOPLOOPINGSOUND,

	CG_CM_TEMPCAPSULEMODEL,
	CG_CM_CAPSULETRACE,
	CG_CM_TRANSFORMEDCAPSULETRACE,
	CG_R_ADDADDITIVELIGHTTOSCENE,
	CG_GET_ENTITY_TOKEN,
	CG_R_ADDPOLYSTOSCENE,
	CG_R_INPVS,
	// 1.32
	CG_FS_SEEK,

	/*
	CG_LOADCAMERA,
	CG_STARTCAMERA,
	CG_GETCAMERAINFO,
	*/

	CG_MEMSET = 100,
	CG_MEMCPY,
	CG_STRNCPY,
	CG_SIN,
	CG_COS,
	CG_ATAN2,
	CG_SQRT,
	CG_FLOOR,
	CG_CEIL,
	CG_TESTPRINTINT,
	CG_TESTPRINTFLOAT,
	CG_ACOS
} cgameImport_t;

/// Functions exported to the main executable.
typedef enum {
	/// Called when the level loads or when the renderer is restarted.
	/// All media should be registered at this time.
	/// cgame will display loading status by calling SCR_Update, which
	/// will call CG_DrawInformation during the loading process
	/// reliableCommandSequence will be 0 on fresh loads, but higher for
	/// demos, tourney restarts, or vid_restarts
	///	void CG_Init( int serverMessageNum, int serverCommandSequence, int clientNum )
	CG_INIT,

	/// Opportunity to flush and close any open files.
	///	void (*CG_Shutdown)( void );
	CG_SHUTDOWN,

	/// A console command has been issued locally that is not recognized by the
	/// main game system.
	/// use Cmd_Argc() / Cmd_Argv() to read the command, return qfalse if the
	/// command is not known to the game
	///	qboolean (*CG_ConsoleCommand)( void );
	CG_CONSOLE_COMMAND,

	/// Generates and draws a game scene and status information at the given time.
	/// If demoPlayback is set, local movement prediction will not be enabled
	///	void (*CG_DrawActiveFrame)( int serverTime, stereoFrame_t stereoView, qboolean demoPlayback );
	CG_DRAW_ACTIVE_FRAME,

	///	int (*CG_CrosshairPlayer)( void );
	CG_CROSSHAIR_PLAYER,

	///	int (*CG_LastAttacker)( void );
	CG_LAST_ATTACKER,

	///	void	(*CG_KeyEvent)( int key, qboolean down );
	CG_KEY_EVENT,

	///	void	(*CG_MouseEvent)( int dx, int dy );
	CG_MOUSE_EVENT,
	///	void (*CG_EventHandling)(int type);
	CG_EVENT_HANDLING
} cgameExport_t;
