/// @file cg_local.h
/// Local definitions for the client game module.

#include "../game/q_shared.h"

#include "../cgame/tr_types.h"
#include "../game/bg_public.h"
#include "cg_public.h"

/// Client game (cg) information.
typedef struct cg_t {
	/// Incremented each frame.
	int clientFrame;
	int clientNum;

	/// The time value that the client is rendering at.
	int time;

	/// View rendering (refresh) definition.
	refdef_t refdef;

	/// Will be converted to refdef.viewaxis
	vec3_t refdefViewAngles;
} cg_t;

/// The client game static (cgs) structure hold everything
/// loaded or calculated from the gamestate.  It will NOT
/// be cleared when a tournament restart is done, allowing
/// all clients to begin playing instantly.
typedef struct cgs_t {
	/// gamestate from server
	gameState_t gameState;
	/// rendering configuration
	glconfig_t glconfig;
	/// reliable command stream counter
	int serverCommandSequence;
	/// the number of snapshots cgame has requested
	int processedSnapshotNum;

	/// Parsed from serverinfo
	/// serverinfo: "mapname"
	char mapName[MAX_QPATH];
} cgs_t;

//
// cg_main.c
//
extern cg_t cg;
extern cgs_t cgs;

//
// cg_main.c
//
void QDECL CG_Printf(const char *msg, ...);
void QDECL CG_Error(const char *msg, ...);

//
// cg_view.c
//
void CG_DrawActiveFrame(int serverTime, stereoFrame_t stereoView, qboolean demoPlayback);

/// @name System Traps
/// These functions are how the cgame communicates with the main game system
/// @{
void trap_Print(const char *fmt);
void trap_Error(const char *fmt);

void trap_GetGameState(gameState_t *gamestate);
void trap_GetGlconfig(glconfig_t *glconfig);

void trap_CM_LoadMap(const char *mapname);

void trap_R_LoadWorldMap(const char *mapname);
void trap_R_ClearScene(void);
void trap_R_RenderScene(const refdef_t *refdef);
/// @}
