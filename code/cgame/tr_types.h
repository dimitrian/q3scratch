/// @file tr_types.h
/// Rendering related types.
/// TR - possibly mean Triangle or Trinity Renderer.

#define MAX_RENDER_STRINGS       8
#define MAX_RENDER_STRING_LENGTH 32

/// Refresh definition.
typedef struct {
	int x, y, width, height;
	float fov_x, fov_y;
	vec3_t vieworg;

	/// Transformation matrix.
	vec3_t viewaxis[3];

	/// Time in milliseconds for shader effects and other time dependent
	/// rendering issues.
	int time;

	/// RDF_NOWORLDMODEL, etc
	int rdflags;

	/// 1 bits will prevent the associated area from rendering at all.
	byte areamask[MAX_MAP_AREA_BYTES];

	/// Text messages for deform text shaders.
	char text[MAX_RENDER_STRINGS][MAX_RENDER_STRING_LENGTH];
} refdef_t;

typedef enum {
	STEREO_CENTER,
	STEREO_LEFT,
	STEREO_RIGHT
} stereoFrame_t;

/// Contains variables specific to the OpenGL configuration
/// being run right now.  These are constant once the OpenGL
/// subsystem is initialized.
typedef enum {
	TC_NONE,
	TC_S3TC,    // this is for the GL_S3_s3tc extension.
	TC_S3TC_ARB // this is for the GL_EXT_texture_compression_s3tc extension.
} textureCompression_t;

typedef enum {
	GLDRV_ICD,        // driver is integrated with window system
	                  // WARNING: there are tests that check for
	                  // > GLDRV_ICD for minidriverness, so this
	                  // should always be the lowest value in this
	                  // enum set
	GLDRV_STANDALONE, // driver is a non-3Dfx standalone driver
	GLDRV_VOODOO      // driver is a 3Dfx standalone driver
} glDriverType_t;

typedef enum {
	GLHW_GENERIC,   // where everything works the way it should
	GLHW_3DFX_2D3D, // Voodoo Banshee or Voodoo3, relevant since if this is
	                // the hardware type then there can NOT exist a secondary
	                // display adapter
	GLHW_RIVA128,   // where you can't interpolate alpha
	GLHW_RAGEPRO,   // where you can't modulate alpha on alpha textures
	GLHW_PERMEDIA2  // where you don't have src*dst
} glHardwareType_t;

/// Graphics Library configuration that includes viewport and renderer info.
typedef struct {
	char renderer_string[MAX_STRING_CHARS];
	char vendor_string[MAX_STRING_CHARS];
	char version_string[MAX_STRING_CHARS];
	char extensions_string[BIG_INFO_STRING];

	/// queried from GL
	int maxTextureSize;
	/// multitexture ability
	int numTextureUnits;

	int colorBits, depthBits, stencilBits;

	glDriverType_t driverType;
	glHardwareType_t hardwareType;

	qboolean deviceSupportsGamma;
	textureCompression_t textureCompression;
	qboolean textureEnvAddAvailable;

	int vidWidth, vidHeight;
	/// aspect is the screen's physical width / height, which may be different
	/// than scrWidth / scrHeight if the pixels are non-square
	/// normal screens should be 4/3, but wide aspect monitors may be 16/9
	float windowAspect;

	int displayFrequency;

	// synonymous with "does rendering consume the entire screen?", therefore
	// a Voodoo or Voodoo2 will have this set to TRUE, as will a Win32 ICD that
	// used CDS.
	qboolean isFullscreen;
	qboolean stereoEnabled;
	/// UNUSED, present for compatibility
	qboolean smpActive;
} glconfig_t;

