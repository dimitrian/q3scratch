/// @file cg_main.c
/// Initialization and primary entry point for cgame.

#include "cg_local.h"

cg_t cg;
cgs_t cgs;

void CG_Init(int serverMessageNum, int serverCommandSequence, int clientNum);
void CG_Shutdown(void);

/// This is the only way control passes into the module.
/// This MUST be the very first function compiled into the .qvm file.
DLLEXPORT intptr_t vmMain(int command, int arg0, int arg1, int arg2) {
	switch (command) {
	case CG_INIT:
		CG_Init(arg0, arg1, arg2);
		return 0;
	case CG_SHUTDOWN:
		CG_Shutdown();
		return 0;
	case CG_CONSOLE_COMMAND:
		return 0;
	case CG_DRAW_ACTIVE_FRAME:
		CG_DrawActiveFrame(arg0, arg1, arg2);
		return 0;
	case CG_CROSSHAIR_PLAYER:
		return 0;
	case CG_LAST_ATTACKER:
		return 0;
	case CG_KEY_EVENT:
		return 0;
	case CG_MOUSE_EVENT:
		return 0;
	case CG_EVENT_HANDLING:
		return 0;
	default:
		CG_Error( "vmMain: unknown command %i", command );
		break;
	}
	return -1;
}

/// This function is liked (referenced) from q_shared.o
void QDECL Com_Error(int level, const char *fmt, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, fmt);
	ED_vsprintf(text, fmt, argptr);
	va_end(argptr);

	trap_Error(text);
}

/// This function is liked (referenced) from q_shared.o
void QDECL Com_Printf(const char *fmt, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, fmt);
	ED_vsprintf(text, fmt, argptr);
	va_end(argptr);

	trap_Print(text);
}

void QDECL CG_Printf(const char *msg, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, msg);
	ED_vsprintf(text, msg, argptr);
	va_end(argptr);

	trap_Print(text);
}

void QDECL CG_Error(const char *msg, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, msg);
	ED_vsprintf(text, msg, argptr);
	va_end(argptr);

	trap_Error(text);
}

const char *CG_ConfigString(int index) {
	if (index < 0 || index >= MAX_CONFIGSTRINGS) {
		CG_Error("CG_ConfigString: bad index: %i", index);
	}
	return cgs.gameState.stringData + cgs.gameState.stringOffsets[index];
}

// This is called explicitly when the gamestate is first received,
// and whenever the server updates any serverinfo flagged cvars.
void CG_ParseServerInfo(void) {
	const char *info;
	char *mapName;

	info = CG_ConfigString(CS_SERVERINFO);

	mapName = Info_ValueForKey(info, "mapname");
	Com_sprintf(cgs.mapName, sizeof(cgs.mapName), "maps/%s.bsp", mapName);
}

/// This function may execute for a couple of minutes with a slow disk.
static void CG_RegisterGraphics(void) {
	// clear any references to old media
	memset(&cg.refdef, 0, sizeof(cg.refdef));
	trap_R_ClearScene();

	// TODO: precache graphics assets

	trap_R_LoadWorldMap(cgs.mapName);
}

/// Called after every level change or subsystem restart.
/// Will perform callbacks to make the loading info screen update.
void CG_Init(int serverMessageNum, int serverCommandSequence, int clientNum) {
	// Clear everything.
	memset(&cg, 0, sizeof(cg));
	memset(&cgs, 0, sizeof(cgs));

	cg.clientNum = clientNum;

	cgs.processedSnapshotNum = serverMessageNum;
	cgs.serverCommandSequence = serverCommandSequence;

	// TODO: load a few needed things before we do any screen updates

	// TODO: register Cvars
	// TODO: init console commands

	// Get the rendering configuration from the client system
	trap_GetGlconfig(&cgs.glconfig);

	// Get the gamestate from the client system
	trap_GetGameState(&cgs.gameState);

	CG_ParseServerInfo();

	CG_Printf("CG_Init: %s\n", cgs.mapName);
	trap_CM_LoadMap(cgs.mapName);

	CG_RegisterGraphics();
}

/// Called before every map change or subsystem restart.
void CG_Shutdown(void) {
	// some mods may need to do cleanup work here,
	// like closing files or archiving session data
}

