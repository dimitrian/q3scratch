/// @file cg_view.c
/// Setup all the parameters (position, angle, etc) for a 3D rendering.

#include "cg_local.h"

/// Set the coordinates of the rendered window.
static void CG_CalcVrect(void) {
	int size = 100;

	cg.refdef.width = cgs.glconfig.vidWidth * size / 100;
	cg.refdef.height = cgs.glconfig.vidHeight * size / 100;
	// Make it even by reset first bit to 0
	cg.refdef.width &= ~1;
	cg.refdef.height &= ~1;

	cg.refdef.x = (cgs.glconfig.vidWidth - cg.refdef.width) / 2;
	cg.refdef.y = (cgs.glconfig.vidHeight - cg.refdef.height) / 2;
}

static qboolean CG_CalcFov(void) {
	float x;
	float fov_x, fov_y;
	fov_x = 90;

	x = cg.refdef.width / tan(fov_x / 360 * M_PI);
	fov_y = atan2(cg.refdef.height, x);
	fov_y = fov_y * 360 / M_PI;

	cg.refdef.fov_x = fov_x;
	cg.refdef.fov_y = fov_y;

	return qtrue;
}

static qboolean CG_CalcViewValues(void) {
	memset(&cg.refdef, 0, sizeof(cg.refdef));

	// Calculate size of 3D view.
	CG_CalcVrect();

	// VectorSet(cg.refdef.vieworg, -48, -192, 80);
	VectorSet(cg.refdef.vieworg, 212, 2360, 56);

	// Position eye relative to origin.
	AnglesToAxis(cg.refdefViewAngles, cg.refdef.viewaxis);

	return CG_CalcFov();
}

/// Generates and draws a game scene and status information at the given time.
void CG_DrawActiveFrame(int serverTime, stereoFrame_t stereoView, qboolean demoPlayback) {
	cg.time = serverTime;

	// Clear all the render lists.
	trap_R_ClearScene();

	// Build cg.refdef
	CG_CalcViewValues();

	// Draw 3D view.
	trap_R_RenderScene(&cg.refdef);
}
