/// @file cg_syscalls.c
/// This file is only included when building a dll.
/// cg_syscalls.asm is included instead when building a qvm

#ifdef Q3_VM
#error "Do not use in VM build"
#endif

#include "cg_local.h"

static dllSyscall_t syscall = (dllSyscall_t)-1;

DLLEXPORT void dllEntry(dllSyscall_t syscallptr) {
	syscall = syscallptr;
}

/// Print message on the local console.
void trap_Print(const char *text) {
	syscall(CG_PRINT, text);
}

/// Abort the game.
void trap_Error(const char *text) {
	syscall(CG_ERROR, text);
}

/// The gamestate should be grabbed at startup, and whenever a
/// configstring changes.
void trap_GetGameState(gameState_t *gamestate) {
	syscall(CG_GETGAMESTATE, gamestate);
}

/// The glconfig_t will not change during the life of a cgame.
/// If it needs to change, the entire cgame will be restarted, because
/// all the qhandle_t are then invalid.
void trap_GetGlconfig(glconfig_t *glconfig) {
	syscall(CG_GETGLCONFIG, glconfig);
}

/// A scene is built up by calls to R_ClearScene and the various R_Add functions.
/// Nothing is drawn until R_RenderScene is called.
void trap_R_ClearScene(void) {
	syscall(CG_R_CLEARSCENE);
}

/// Render 3D scene.
void trap_R_RenderScene(const refdef_t *fd) {
	syscall(CG_R_RENDERSCENE, fd);
}

/// Loads world for the render scene.
/// Otherwise renderer will stop with an error "NULL worldmodel".
void trap_R_LoadWorldMap(const char *mapname) {
	syscall(CG_R_LOADWORLDMAP, mapname);
}

/// Loads Collision Model map.
void trap_CM_LoadMap(const char *mapname) {
	syscall(CG_CM_LOADMAP, mapname);
}
