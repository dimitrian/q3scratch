/// @file g_local.h
/// Local definitions for game module.

/// @defgroup _g_vm_syscalls Game VM System Calls
/// These functions are how the server system communicates with the game.
/// Engine sends messages to Server VM module by calling #vmMain.
/// The list of Messages that can be sent to the Server VM is in #gameExport_t.

#include "q_shared.h"

#include "bg_public.h"
#include "g_public.h"

/// The "gameversion" client command will print this plus compile date.
#define GAMEVERSION "q3scratch"

typedef enum gclientConnection_t {
	CON_DISCONNECTED,
	CON_CONNECTING,
	CON_CONNECTED
} gclientConnection_t;

/// Client data that stays across multiple respawns, but is cleared
/// on each level change or team change at G_ClientBegin()
typedef struct gclientPersistent_t {
	gclientConnection_t connection;
} gclientPersistent_t;

/// The player or bot data.
/// This structure is cleared on each ClientSpawn(),
/// except for 'client->pers' and 'client->sess'.
typedef struct gclient_t {
	/// @name Shared Header
	/// @{
	/// Included in the snapshot communicated by server to clients.
	/// MUST be the first element, because the server expects it.
	playerState_t ps;
	/// @}
	gclientPersistent_t pers;
} gclient_t;

/// Game Entity.
typedef struct gentity_t {
	/// @name Shared Header
	/// The server looks at a #sharedEntity_t, which is the start(header)
	/// of the game's #gentity_t structure.
	/// @{
	/// Included in the snapshot communicated by the server to clients (#sharedEntity_t::s).
	entityState_t s;
	/// Shared by both the server system and game (#sharedEntity_t::r).
	entityShared_t r;
	// DO NOT MODIFY ANYTHING ABOVE THIS, THE SERVER
	// EXPECTS THE FIELDS IN THAT ORDER!
	// ================================
	/// @}

	gclient_t *client; ///< NULL if not a client.
	char *classname;
	qboolean inUse;
} gentity_t;

/// Map related information with references to entities and clients.
/// This structure is cleared as each map is entered.
typedef struct gmap_t {
	gclient_t *clients; ///< [maxclients]
	int maxClients;     ///< cvar g_maxclients

	gentity_t *entities;
	int numEntities; ///< MAX_CLIENTS <= entityCount <= ENTITYNUM_MAX_NORMAL

	int time;      ///< in msec
	int startTime; ///< time the map was started

	fileHandle_t logFile;
} gmap_t;

extern gmap_t map;

//
// g_client.c
//
char *G_ClientConnect(int clientNum, qboolean firstTime, qboolean isBot);
void G_ClientDisconnect(int clientNum);
void G_ClientBegin(int clientNum);

//
// g_utils.c
//
void QDECL G_LogPrintf(const char *fmt, ...);
void QDECL G_Printf(const char *fmt, ...);
void G_InitEntity(gentity_t *ent);
void G_SetOrigin(gentity_t *ent, vec3_t origin);

/// @name System Traps
/// These functions are how the game communicates with the server system
/// @{
void trap_Print(const char *fmt);
void trap_Error(const char *fmt);
void trap_LocateGameData(gentity_t *gEnts, int numGEntities, int sizeofGEntity_t, playerState_t *clients, int sizeofGClient);
void trap_UnlinkEntity(gentity_t *ent);
void trap_GetUserinfo(int num, char *buffer, int bufferSize);
void trap_FS_Write(const void *buffer, int len, fileHandle_t f);
/// @}
