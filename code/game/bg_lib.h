/// @file bg_lib.h
/// Standard C library replacement routines used by code
/// compiled for the virtual machine.
///
/// This file is NOT included on native builds.

typedef int size_t;
typedef int intptr_t;

typedef char *va_list;
#define _INTSIZEOF(n)   ((sizeof(n) + sizeof(int) - 1) & ~(sizeof(int) - 1))
#define va_start(ap, v) (ap = (va_list)&v + _INTSIZEOF(v))
#define va_arg(ap, t)   (*(t *)((ap += _INTSIZEOF(t)) - _INTSIZEOF(t)))
#define va_end(ap)      (ap = (va_list)0)

#define CHAR_BIT  8      /* number of bits in a char */
#define SCHAR_MIN (-128) /* minimum signed char value */
#define SCHAR_MAX 127    /* maximum signed char value */
#define UCHAR_MAX 0xff   /* maximum unsigned char value */

#define SHRT_MIN  (-32768)           /* minimum (signed) short value */
#define SHRT_MAX  32767              /* maximum (signed) short value */
#define USHRT_MAX 0xffff             /* maximum unsigned short value */
#define INT_MIN   (-2147483647 - 1)  /* minimum (signed) int value */
#define INT_MAX   2147483647         /* maximum (signed) int value */
#define UINT_MAX  0xffffffff         /* maximum unsigned int value */
#define LONG_MIN  (-2147483647L - 1) /* minimum (signed) long value */
#define LONG_MAX  2147483647L        /* maximum (signed) long value */
#define ULONG_MAX 0xffffffffUL       /* maximum unsigned long value */

/// Function that compares two elements for #qsort.
typedef int cmp_t(const void *, const void *);

/// @name Misc functions
/// @{

/// Sorts the given array pointed to by ptr in ascending order. 
/// The array contains count elements of size bytes. 
/// Function pointed to by cmp is used for object comparison.
void qsort(void *ptr, size_t count, size_t size, cmp_t *cmp);

/// Seeds the pseudo-random number generator used by #rand() with the value seed.
void srand(unsigned seed);

/// Returns a pseudo-random integer value.
int rand(void);
/// @}

/// @name String functions
/// @{
size_t strlen(const char *string);
char *strcat(char *strDestination, const char *strSource);
char *strcpy(char *strDestination, const char *strSource);
int strcmp(const char *string1, const char *string2);
char *strchr(const char *string, int c);
char *strstr(const char *string, const char *strCharSet);
char *strncpy(char *strDest, const char *strSource, size_t count);
int tolower(int c);
int toupper(int c);

double atof(const char *string);
int atoi(const char *string);

/// @}

/// @name Memory functions
/// @{
void *memmove(void *dest, const void *src, size_t count);
void *memset(void *dest, int c, size_t count);
void *memcpy(void *dest, const void *src, size_t count);
/// @}

/// @name Math functions
/// @{
double ceil(double x);
double floor(double x);
double sqrt(double x);
double sin(double x);
double cos(double x);
double atan2(double y, double x);
double tan(double x);
int abs(int n);
double fabs(double x);
double acos(double x);
/// @}

