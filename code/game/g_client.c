/// @file g_client.c
/// Cient functions that don't happen every frame.
///
/// This file functions to spawn the client and handle the
/// connection with the server.

#include "g_local.h"

static vec3_t playerMins = {-15, -15, -24};
static vec3_t playerMaxs = {15, 15, 32};

/// Called when a player begins connecting to the server.
/// Called again for every map change or tournement restart.
///
/// The session information will be valid after exit.
///
/// @return Return NULL if the client should be allowed, otherwise return
/// a string with the reason for denial.
/// Otherwise, the client will be sent the current gamestate
/// and will eventually get to #G_ClientBegin.
///
/// @param clientNum client number
/// @param firstTime will be qtrue the very first time a client connects
/// to the server machine, but qfalse on map changes.
/// @param isBot whether the client is a bot
/// @ingroup _g_vm_syscalls
char *G_ClientConnect(int clientNum, qboolean firstTime, qboolean isBot) {
	gentity_t *ent;
	gclient_t *client;
	char userinfo[MAX_INFO_STRING];

	ent = &map.entities[clientNum];

	// e.g.: \ip\localhost\challenge\0\qport\51597\protocol\71\name\UnnamedPlayer...
	trap_GetUserinfo(clientNum, userinfo, sizeof(userinfo));

	// if a player reconnects quickly after a disconnect, the client disconnect
	// may never be called, thus flag can get lost in the ether
	if (ent->inUse) {
		G_LogPrintf("Forcing disconnect on active client: %i\n", clientNum);
		// so lets just fix up anything that should happen on a disconnect
		G_ClientDisconnect(clientNum);
	}

	// Client can connect.
	ent->client = &map.clients[clientNum];
	client = ent->client;

	memset(client, 0, sizeof(*client));
	client->pers.connection = CON_CONNECTING;

	G_LogPrintf("ClientConnect: %i\n", clientNum);

	return NULL;
}

/// Called when a player drops from the server.
/// Will not be called between maps.
///
/// This should NOT be called directly by any game logic,
/// call #trap_DropClient(), which will call this and do
/// server system housekeeping.
/// @ingroup _g_vm_syscalls
void G_ClientDisconnect(int clientNum) {
	gentity_t *ent;

	ent = &map.entities[clientNum];
	if (!ent->client || ent->client->pers.connection == CON_DISCONNECTED) {
		return;
	}

	G_LogPrintf("ClientDisconnect: %i\n", clientNum);

	trap_UnlinkEntity(ent);

	ent->s.modelindex = 0;
	ent->inUse = qfalse;
	ent->classname = "disconnected";
	ent->client->pers.connection = CON_DISCONNECTED;
}

/// Set view angle for the client.
/// @param ent client entity
/// @param angle view angle to set
void G_ClientSetViewAngle(gentity_t *ent, vec3_t angle) {
	// TODO: Add this when usercmd implemented
	// int i, cmdAngle;
	// gclient_t *client;

	// client = ent->client;
	// // Set the delta angle.
	// for (i = 0; i < 3; i++) {
	// 	cmdAngle = ANGLE2SHORT(angle[i]);
	// 	client->ps.delta_angles[i] = cmdAngle - client->pers.cmd.angles[i];
	// }
	VectorCopy(angle, ent->s.angles);
	VectorCopy(ent->s.angles, ent->client->ps.viewangles);
}

/// Called every time a client is placed fresh in the world:
/// after the first #G_ClientBegin, and after each respawn.
/// Initializes all non-persistent parts of #playerState_t
void G_ClientSpawn(gentity_t *ent) {
	gclient_t *client;
	gclientPersistent_t pers;
	int persistent[MAX_PERSISTENT];
	int eventSequence;
	vec3_t spawnOrigin;
	vec3_t spawnAngles;
	int flags;
	int i;

	client = ent->client;
	VectorSet(spawnOrigin, -48, 192, 80);
	VectorClear(spawnAngles);

	flags = client->ps.eFlags;

	// Save all persistent client info.
	eventSequence = client->ps.eventSequence;
	pers = client->pers;
	for (i = 0; i < MAX_PERSISTENT; i++) {
		persistent[i] = client->ps.persistent[i];
	}

	// Clear everything but the persistent data.
	memset(client, 0, sizeof(*client));

	// Restore all persistent client info.
	client->pers = pers;
	client->ps.eventSequence = eventSequence;
	client->ps.eFlags = flags;
	client->ps.clientNum = ent->s.number;
	for (i = 0; i < MAX_PERSISTENT; i++) {
		client->ps.persistent[i] = persistent[i];
	}

	ent->classname = "player";
	ent->inUse = qtrue;
	ent->s.groundEntityNum = ENTITYNUM_NONE;
	ent->r.contents = CONTENTS_BODY;

	VectorCopy(playerMins, ent->r.mins);
	VectorCopy(playerMaxs, ent->r.maxs);

	G_SetOrigin(ent, spawnOrigin);
	// TODO: why not part of G_SetOrigin?
	VectorCopy(spawnOrigin, client->ps.origin);

	G_ClientSetViewAngle(ent, spawnAngles);
}

/// Called when a client has finished connecting, and is ready
/// to be placed into the map. This will happen every map load,
/// and on transition between teams, but doesn't happen on respawns
/// @ingroup _g_vm_syscalls
void G_ClientBegin(int clientNum) {
	gentity_t *ent;
	gclient_t *client;
	int flags;

	ent = &map.entities[clientNum];
	client = &map.clients[clientNum];

	if (ent->r.linked) {
		trap_UnlinkEntity(ent);
	}

	G_InitEntity(ent);
	ent->client = client;
	client->pers.connection = CON_CONNECTED;

	// Save eFlags around this, because changing teams will
	// cause this to happen with a valid entity, and we
	// want to make sure the teleport bit is set right
	// so the viewpoint doesn't interpolate through the
	// world to the new position.
	flags = client->ps.eFlags;
	memset(&client->ps, 0, sizeof(client->ps));
	client->ps.eFlags = flags;

	G_Printf("ClientBegin: %d\n", clientNum);
}

