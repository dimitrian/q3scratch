/// @file g_main.c
/// VM entry point which is responsible for handling calls from server system into VM.

#include "g_local.h"

gmap_t map;
gentity_t g_entities[MAX_GENTITIES];
gclient_t g_clients[MAX_CLIENTS];

void G_Init(int serverTime, int randomSeed, int restart);
void G_Shutdown(qboolean restart);

/// This is the only way control passes into the module.
/// This MUST be the very first function compiled into the .qvm file.
/// The list of Messages that can be sent to the Server VM is in #gameExport_t.
/// @anchor _g_vm_main
/// @callgraph
DLLEXPORT intptr_t vmMain(int command, int arg0, int arg1, int arg2) {
	switch (command) {
	case GAME_INIT:
		G_Init(arg0, arg1, arg2);
		return 0;
	case GAME_SHUTDOWN:
		G_Shutdown(arg0);
		return 0;
	case GAME_CLIENT_CONNECT:
		// Returns a string - (char *) and passes it back to engine as int.
		return (intptr_t)G_ClientConnect(arg0, arg1, arg2);
	case GAME_CLIENT_THINK:
		return 0;
	case GAME_CLIENT_USERINFO_CHANGED:
		return 0;
	case GAME_CLIENT_DISCONNECT:
		G_ClientDisconnect(arg0);
		return 0;
	case GAME_CLIENT_BEGIN:
		G_ClientBegin(arg0);
		return 0;
	case GAME_CLIENT_COMMAND:
		return 0;
	case GAME_RUN_FRAME:
		return 0;
	case GAME_CONSOLE_COMMAND:
		return 0;
	case BOTAI_START_FRAME:
		return 0;
	}

	return -1;
}

/// Initialize the game.
/// @ingroup _g_vm_syscalls
void G_Init(int serverTime, int randomSeed, int restart) {
	int i;

	G_Printf("------- Game Initialization -------\n");
	G_Printf("gamename: %s\n", GAMEVERSION);
	G_Printf("gamedate: %s\n", __DATE__);

#if Q3_VM
	G_Printf("G_INIT: QVM mode\n");
#else
	G_Printf("G_INIT: native mode\n");
#endif

	// Server uses the current msec count for a random seed.
	srand(randomSeed);

	// Initialize map.
	memset(&map, 0, sizeof(map));
	map.time = serverTime;
	map.startTime = serverTime;

	// Initialize all entities for this game.
	memset(g_entities, 0, MAX_GENTITIES * sizeof(g_entities[0]));
	map.entities = g_entities;

	// Initialize all clients for this game.
	// TODO: level.maxclients = g_maxclients.integer;
	// for now set it to MAX_CLIENTS:
	map.maxClients = MAX_CLIENTS;
	memset(g_clients, 0, MAX_CLIENTS * sizeof(g_clients[0]));
	map.clients = g_clients;

	// Set client fields on player ents.
	for (i = 0; i < map.maxClients; i++) {
		map.entities[i].client = map.clients + i;
	}

	// Always leave room for the max number of clients,
	// even if they aren't all used, so numbers inside that
	// range are NEVER anything but clients.
	map.numEntities = MAX_CLIENTS;

	for (i = 0; i < MAX_CLIENTS; i++) {
		map.entities[i].classname = "clientslot";
	}

	// Let the server system know where the entities are.
	trap_LocateGameData(map.entities, map.numEntities, sizeof(gentity_t),
	                    &map.clients[0].ps, sizeof(gclient_t));

	G_Printf("-----------------------------------\n");
}

/// Close logs and write session data.
/// @ingroup _g_vm_syscalls
void G_Shutdown(qboolean restart) {
	G_Printf("==== Game Shutdown ====\n");
}
