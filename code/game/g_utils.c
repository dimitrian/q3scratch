/// @file g_utils.c
/// Misc utility functions for game module.

#include "g_local.h"

/// This function is liked (referenced) from q_shared.o (*.h)
void QDECL Com_Error(int level, const char *fmt, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, fmt);
	ED_vsprintf(text, fmt, argptr);
	va_end(argptr);

	trap_Error(text);
}

/// This function is liked (referenced) from q_shared.o (*.h)
void QDECL Com_Printf(const char *fmt, ...) {
	va_list argptr;
	char text[1024];

	va_start(argptr, fmt);
	ED_vsprintf(text, fmt, argptr);
	va_end(argptr);

	trap_Print(text);
}

void QDECL G_Printf(const char *fmt, ...) {
	// Create a va_list type variable in the function definition.
	// This type is defined in stdarg.h header file.
	// Redefined for QVM in bg_lib.h
	va_list argptr;
	char text[1024];

	// Initialize valist for num number of arguments.
	// Redefined for QVM. */
	va_start(argptr, fmt);
	// Access all the arguments assigned to valist.
	ED_vsprintf(text, fmt, argptr);
	// Clean memory reserved for valist.
	// Redefined for QVM.
	va_end(argptr);

	trap_Print(text);
}

/// Print to the logfile with a time stamp if it is open
void QDECL G_LogPrintf(const char *fmt, ...) {
	va_list argptr;
	char string[1024];
	int min, tens, sec, len;

	sec = (map.time - map.startTime) / 1000;

	min = sec / 60;
	sec -= min * 60;
	tens = sec / 10;
	sec -= tens * 10;

	// Write the timestamp first "123:45 " size = 7
	len = Com_sprintf(string, sizeof(string), "%3i:%i%i ", min, tens, sec);

	va_start(argptr, fmt);
	ED_vsprintf(string + len, fmt, argptr);
	va_end(argptr);

	// TODO: add cvar
	// if (g_dedicated.integer) {
	// 	G_Printf("%s", string + 7);
	// }
	G_Printf("%s", string);

	if (!map.logFile) {
		return;
	}
	trap_FS_Write(string, strlen(string), map.logFile);
}

/// Prepare entity for further usage.
void G_InitEntity(gentity_t *ent) {
	ent->inUse = qtrue;
	ent->classname = "noclass";
	ent->s.number = ent - map.entities;
	ent->r.ownerNum = ENTITYNUM_NONE;
}

/// Set position to #TR_STATIONARY trajectory.
void G_SetOrigin(gentity_t *ent, vec3_t origin) {
	VectorCopy(origin, ent->s.pos.trBase);
	ent->s.pos.trType = TR_STATIONARY;
	ent->s.pos.trTime = 0;
	ent->s.pos.trDuration = 0;
	VectorClear(ent->s.pos.trDelta);
	
	VectorCopy(origin, ent->r.currentOrigin);
}
