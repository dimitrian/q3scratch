/// @file bg_public.h
/// Definitions shared by both the server game and client game modules.
///
/// The server and client system include and use the information defined in this file!

/// #entityState_t->eType
typedef enum {
	ET_GENERAL,
	ET_EVENTS // any of the EV_* events can be added freestanding
	          // by setting eType to ET_EVENTS + eventNum
	          // this avoids having to set eFlags and eventNum
} entityType_t;

