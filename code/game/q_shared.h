/// @file q_shared.h
/// Included first by ALL program modules.
/// A user mod should never modify this file.

#ifndef __Q_SHARED_H
#define __Q_SHARED_H

#include "surfaceflags.h"

/**********************************************************************
  VM Considerations

  The VM can not use the standard system headers because we aren't really
  using the compiler they were meant for.  We use bg_lib.h which contains
  prototypes for the functions we define for our own use in bg_lib.c.

  When writing mods, please add needed headers HERE, do not start including
  stuff like <stdio.h> in the various .c files that make up each of the VMs
  since you will be including system headers files can will have issues.

  Remember, if you use a C library function that is not defined in bg_lib.c,
  you will have to add your own version for support in the VM.

 **********************************************************************/

#ifdef Q3_VM

#include "bg_lib.h"
#define DLLEXPORT

#else

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <math.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifndef _WIN32
#include <stdint.h>
#define DLLEXPORT __attribute__((visibility("default")))
#else
#define DLLEXPORT __declspec(dllexport)
#endif /* _WIN32 */

#endif /* !Q3_VM */

#define QDECL

//======================= WIN32 DEFINES =================================

#ifdef WIN32

#define MAC_STATIC

#undef QDECL
#define QDECL __cdecl

#define ID_INLINE __inline

#define PATH_SEP '\\'

#endif

//======================= LINUX DEFINES =================================

// The mac compiler can't handle >32k of locals, so we
// just waste space and make big arrays static...
#ifdef __linux__

/* bk001205 - from Makefile */
#define stricmp strcasecmp

#define ID_INLINE inline

#define PATH_SEP '/'

#endif

//======================= FreeBSD DEFINES ===============================
#ifdef __FreeBSD__ /* rb010123 */

#define stricmp strcasecmp

#define MAC_STATIC
#define ID_INLINE inline

#define PATH_SEP '/'

#endif

//=============================================================

typedef unsigned char byte;

typedef enum {
	qfalse = 0,
	qtrue
} qboolean;

typedef int fileHandle_t;

#ifndef NULL
#define NULL ((void *)0)
#endif

// angle indexes
#define PITCH 0 ///< up / down
#define YAW   1 ///< left / right
#define ROLL  2 ///< fall over

// the game guarantees that no string from the network will ever
// exceed MAX_STRING_CHARS
#define MAX_STRING_CHARS  1024 // max length of a string passed to Cmd_TokenizeString
#define MAX_STRING_TOKENS 1024 // max tokens resulting from Cmd_TokenizeString
#define MAX_TOKEN_CHARS   1024 ///< max length of an individual token

#define MAX_INFO_STRING 1024
#define MAX_INFO_KEY    1024
#define MAX_INFO_VALUE  1024

#define BIG_INFO_STRING 8192 ///< used for system info key only
#define BIG_INFO_KEY    8192
#define BIG_INFO_VALUE  8192

#define MAX_QPATH 64 ///< max length of a quake game pathname

#define MAX_MAP_AREA_BYTES 32 ///< bit vector of area visibility

#ifdef ERR_FATAL
#undef ERR_FATAL ///< this is be defined in malloc.h
#endif

/// Parameters to the main Error routine.
typedef enum {
	ERR_FATAL,            ///< exit the entire game with a popup window
	ERR_DROP,             ///< print to console and disconnect from game
	ERR_SERVERDISCONNECT, ///< don't kill server
	ERR_DISCONNECT,       ///< client disconnected from the server
} errorParm_t;

//===================================================================

/// @name Callbacks to functions defined in game/cgame
/// Functions in q_shared.c and bg_*.c call those functions.
/// This is only here so the functions in q_shared.c and bg_*.c can link
/// @{
void QDECL Com_Error(int level, const char *fmt, ...);
void QDECL Com_Printf(const char *fmt, ...);
/// @}

/// @name Custom functions
/// @{
int ED_vsprintf(char *buffer, const char *fmt, va_list argptr);
/// @}

int QDECL Com_sprintf(char *dest, int size, const char *fmt, ...);

/*
==============================================================

MATHLIB

==============================================================
*/

typedef float vec_t;
typedef vec_t vec2_t[2];
typedef vec_t vec3_t[3];
typedef vec_t vec4_t[4];
typedef vec_t vec5_t[5];

typedef int fixed4_t;
typedef int fixed8_t;
typedef int fixed16_t;

#ifndef M_PI
#define M_PI 3.14159265358979323846f ///< matches value in gcc v2 math.h
#endif

extern vec3_t vec3_origin;

#define VectorClear(a)          ((a)[0] = (a)[1] = (a)[2] = 0)
#define VectorSubtract(a, b, c) ((c)[0] = (a)[0] - (b)[0], (c)[1] = (a)[1] - (b)[1], (c)[2] = (a)[2] - (b)[2])
#define VectorCopy(a, b)        ((b)[0] = (a)[0], (b)[1] = (a)[1], (b)[2] = (a)[2])
#define VectorSet(v, x, y, z)   ((v)[0] = (x), (v)[1] = (y), (v)[2] = (z))

//
// g_math.c
//
void AnglesToAxis(const vec3_t angles, vec3_t axis[3]);

//=============================================
/// @name Syscall Function Pointer Types
///
/// Used for communication between VMs and engine
/// @{

typedef intptr_t (*syscall_t)(intptr_t *parms);
typedef intptr_t(QDECL *dllSyscall_t)(intptr_t callNum, ...);
typedef void(QDECL *dllEntry_t)(dllSyscall_t syscallptr);

/// @}

//=============================================
/// @name Key/Value info strings
/// @{
char *Info_ValueForKey(const char *s, const char *key);
/// @}

/**
========================================================================

@defgroup _sent_across_net Elements Communicated Across the Net

========================================================================
*/

#define SNAPFLAG_RATE_DELAYED 1
#define SNAPFLAG_NOT_ACTIVE   2 // snapshot used during connection and for zombies
#define SNAPFLAG_SERVERCOUNT  4 // toggled every map_restart so transitions can be detected

/// @name per-map limits
/// @{
#define MAX_CLIENTS 64 ///< absolute limit

#define GENTITYNUM_BITS 10 // don't need to send any more
#define MAX_GENTITIES   (1 << GENTITYNUM_BITS)

// entitynums are communicated with GENTITY_BITS, so any reserved
// values that are going to be communcated over the net need to
// also be in this range
#define ENTITYNUM_NONE       (MAX_GENTITIES - 1)
#define ENTITYNUM_WORLD      (MAX_GENTITIES - 2)
#define ENTITYNUM_MAX_NORMAL (MAX_GENTITIES - 2)

#define MAX_CONFIGSTRINGS 1024

/// @} per-map limits

// these are the only configstrings that the system reserves, all the
// other ones are strictly for servergame to clientgame communication
#define CS_SERVERINFO 0 // an info string with all the serverinfo cvars
#define CS_SYSTEMINFO 1 // an info string for server system to client system configuration (timescale, etc)

#define RESERVED_CONFIGSTRINGS 2 // game can't modify below this, only the system can

#define MAX_GAMESTATE_CHARS 16000
/// Game state string data.
typedef struct {
	int stringOffsets[MAX_CONFIGSTRINGS];
	char stringData[MAX_GAMESTATE_CHARS];
	int dataCount;
} gameState_t;

/*=========================================================*/

// bit field limits
#define MAX_STATS      16
#define MAX_PERSISTENT 16
#define MAX_POWERUPS   16
#define MAX_WEAPONS    16

#define MAX_PS_EVENTS 2

/// Part of the #gclient_t needed by both the client and server
/// to predict player motion and actions.
///
/// Nothing outside of pmove should modify these, or some degree of prediction error
/// will occur.
///
/// You can't add anything to this without modifying the code in msg.c
///
/// #playerState_t is a full superset of #entityState_t as it is used by players,
/// so if a #playerState_t is transmitted, the #entityState_t can be fully derived
/// from it.
/// @ingroup _sent_across_net
typedef struct playerState_t {
	int commandTime; ///< cmd->serverTime of last executed command
	int pm_type;
	int bobCycle; ///< for view bobbing and footstep generation
	int pm_flags; ///< ducked, jump_held, etc
	int pm_time;

	vec3_t origin;
	vec3_t velocity;
	int weaponTime;
	int gravity;
	int speed;

	/// add to command angles to get view direction
	/// changed by spawns, rotating objects, and teleporters
	int delta_angles[3];

	int groundEntityNum; ///< ENTITYNUM_NONE = in air

	int legsTimer; ///< don't change low priority animations until this runs out
	int legsAnim;  ///< mask off ANIM_TOGGLEBIT

	int torsoTimer; ///< don't change low priority animations until this runs out
	int torsoAnim;  ///< mask off ANIM_TOGGLEBIT

	/// a number 0 to 7 that represents the relative angle
	/// of movement to the view angle (axial and diagonals)
	/// when at rest, the value will remain unchanged
	/// used to twist the legs during strafing
	int movementDir;

	vec3_t grapplePoint; ///< location of grapple to pull towards if PMF_GRAPPLE_PULL

	int eFlags; ///< copied to entityState_t->eFlags

	int eventSequence; ///< pmove generated events
	int events[MAX_PS_EVENTS];
	int eventParms[MAX_PS_EVENTS];

	int externalEvent; ///< events set on player from another source
	int externalEventParm;
	int externalEventTime;

	int clientNum; ///< ranges from 0 to MAX_CLIENTS-1
	int weapon;    ///< copied to entityState_t->weapon
	int weaponstate;

	vec3_t viewangles; ///< for fixed views
	int viewheight;

	/// @name Damage feedback
	/// @{
	int damageEvent; ///< when it changes, latch the other parms
	int damageYaw;
	int damagePitch;
	int damageCount;
	/// @}

	int stats[MAX_STATS];
	int persistent[MAX_PERSISTENT]; ///< stats that aren't cleared on death
	int powerups[MAX_POWERUPS];     ///< level.time that the powerup runs out
	int ammo[MAX_WEAPONS];

	int generic1;
	int loopSound;
	int jumppad_ent; ///< jumppad entity hit this frame

	/// @name Not communicated over the net at all
	/// @{
	int ping;             ///< server to game info for scoreboard
	int pmove_framecount; ///< FIXME: don't transmit over the network
	int jumppad_frame;
	int entityEventSequence;
	/// @}
} playerState_t;

//===================================================================

/// if entityState->solid == SOLID_BMODEL, modelindex is an inline model number
#define SOLID_BMODEL 0xffffff

/// Trajectory type.
typedef enum {
	TR_STATIONARY,  ///< Stationary not moving
	TR_INTERPOLATE, ///< non-parametric, but interpolate between snapshots
	TR_LINEAR,      ///< Linear movement
	TR_LINEAR_STOP, ///< Linear movement with stop
	TR_SINE,        ///< value = base + sin( time / duration ) * delta
	TR_GRAVITY      ///< Movement accordingly to the gravity
} trType_t;

/// Movement trajectory.
typedef struct {
	trType_t trType;
	int trTime;
	int trDuration; ///< if non 0, trTime + trDuration = stop time
	vec3_t trBase;
	vec3_t trDelta; ///< velocity, etc
} trajectory_t;

/// Part of the #gentity_t conveyed from the server
/// in an update message about entities that the client will
/// need to render in some way.
/// Different eTypes may use the information in different ways
/// The messages are delta compressed, so it doesn't really matter if
/// the structure size is fairly large.
/// @ingroup _sent_across_net
typedef struct entityState_s {
	int number; ///< Entity index
	int eType;  ///< One of the #entityType_t
	int eFlags;

	trajectory_t pos;  ///< for calculating position
	trajectory_t apos; ///< for calculating angles

	int time;
	int time2;

	vec3_t origin;
	vec3_t origin2;

	vec3_t angles;
	vec3_t angles2;

	int otherEntityNum; ///< shotgun sources, etc
	int otherEntityNum2;

	int groundEntityNum; //< ENTITYNUM_NONE = in air

	int constantLight; ///< r + (g<<8) + (b<<16) + (intensity<<24)
	int loopSound;     ///< constantly loop this sound

	int modelindex;
	int modelindex2;
	int clientNum; ///< 0 to (MAX_CLIENTS - 1), for players and corpses
	int frame;

	int solid; ///< for client side prediction, trap_linkentity sets this properly

	int event; ///< impulse events -- muzzle flashes, footsteps, etc
	int eventParm;

	/// @name For players
	/// @{
	int powerups;  ///< bit flags
	int weapon;    ///< determines weapon and flash model, etc
	int legsAnim;  ///< mask off ANIM_TOGGLEBIT
	int torsoAnim; ///< mask off ANIM_TOGGLEBIT
	/// @}

	int generic1;
} entityState_t;

#endif // __Q_SHARED_H

