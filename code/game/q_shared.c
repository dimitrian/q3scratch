/// @file q_shared.c
/// Stateless support routines that are included in each code dll.

#include "q_shared.h"

/*
============================================================================

LIBRARY REPLACEMENT FUNCTIONS

============================================================================
*/

int QDECL Com_sprintf(char *dest, int size, const char *fmt, ...) {
	va_list argptr;
	int len;

	va_start(argptr, fmt);
	len = ED_vsprintf(dest, fmt, argptr);
	va_end(argptr);

	if (len >= size) {
		Com_Error(ERR_FATAL, "Com_sprintf: overflow of %i in %i\n", len, size);
	}

	return len;
}

/// Compare Strings without Case Sensitivity
int Q_stricmpn(const char *s1, const char *s2, int n) {
	int c1, c2;

	if (s1 == NULL) {
		if (s2 == NULL) {
			return 0;
		} else {
			return -1;
		}
	} else if (s2 == NULL) {
		return 1;
	}

	do {
		c1 = *s1++;
		c2 = *s2++;

		if (!n--) {
			// string are equal until end point
			return 0;
		}

		if (c1 != c2) {
			if (c1 >= 'a' && c1 <= 'z') {
				// Convert lowercase chars to uppercase
				// 'a':61 'A':41
				c1 -= ('a' - 'A');
			}
			if (c2 >= 'a' && c2 <= 'z') {
				c2 -= ('a' - 'A');
			}
			if (c1 != c2) {
				return c1 < c2 ? -1 : 1;
			}
		}
	} while (c1);

	// strings are equal
	return 0;
}

int Q_stricmp(const char *s1, const char *s2) {
	return (s1 && s2) ? Q_stricmpn(s1, s2, 99999) : -1;
}

/*
===========================================================================

INFO STRINGS

===========================================================================
*/

/// Searches the string for the given
/// key and returns the associated value, or an empty string.
/// FIXME: overflow check?
char *Info_ValueForKey(const char *s, const char *key) {
	char pkey[BIG_INFO_KEY];
	static char value[2][BIG_INFO_VALUE]; // use two buffers so compares
	                                      // work without stomping on each other
	static int valueindex = 0;
	char *o;

	if (!s || !key) {
		return "";
	}

	if (strlen(s) >= BIG_INFO_STRING) {
		Com_Error(ERR_DROP, "Info_ValueForKey: oversize infostring");
	}

	valueindex ^= 1;
	if (*s == '\\')
		s++;
	while (1) {
		o = pkey;
		while (*s != '\\') {
			if (!*s) {
				return "";
			}
			*o++ = *s++;
		}
		*o = 0;
		s++;

		o = value[valueindex];

		while (*s != '\\' && *s) {
			*o++ = *s++;
		}
		*o = 0;

		if (!Q_stricmp(key, pkey)) {
			return value[valueindex];
		}

		if (!*s) {
			break;
		}
		s++;
	}

	return "";
}

/*
============================================================================

ED_vsprintf implementation

============================================================================
*/
#define ALT       0x00000001 /* alternate form */
#define HEXPREFIX 0x00000002 /* add 0x or 0X prefix */
#define LADJUST   0x00000004 /* left adjustment */
#define LONGDBL   0x00000008 /* long double */
#define LONGINT   0x00000010 /* long integer */
#define QUADINT   0x00000020 /* quad integer */
#define SHORTINT  0x00000040 /* short integer */
#define ZEROPAD   0x00000080 /* zero (as opposed to blank) pad */
#define FPT       0x00000100 /* floating point number */
#define REDUCE    0x00000200 /* extension: do not emit anything if value is zero */

#define to_digit(c) ((c) - '0')
#define is_digit(c) ((unsigned)to_digit(c) <= 9)
#define to_char(n)  ((n) + '0')

static void AddInt(char **buf_p, int signedVal, int width, int flags) {
	char text[32];
	int digits;
	unsigned val;
	char *buf;

	if (signedVal < 0) {
		// NOTE: if val is "int" then "-INT_MIN" will overflow
		// make sure val has space in this case it has type "unsigned"
		val = -signedVal;
	} else {
		val = signedVal;
	}

	if (flags & REDUCE && val == 0)
		return;

	digits = 0;
	do {
		text[digits] = '0' + val % 10;
		digits++;
		val /= 10;
	} while (val);

	if (signedVal < 0) {
		text[digits] = '-';
		digits++;
	}

	buf = *buf_p;

	if (!(flags & LADJUST)) {
		while (digits < width) {
			*buf = (flags & ZEROPAD) ? '0' : ' ';
			buf++;
			width--;
		}
	}

	while (digits-- > 0) {
		*buf = text[digits];
		buf++;
		width--;
	}

	if (flags & LADJUST) {
		while (width > 0) {
			*buf = (flags & ZEROPAD) ? '0' : ' ';
			buf++;
			width--;
		}
	}

	*buf_p = buf;
}

static void AddFloat(char **buf_p, float fval, int width, int prec, int reduce) {
	char text[32];
	int digits;
	float signedVal;
	char *buf;
	int val;

	if (reduce && fval == 0.0f)
		return;

	// get the sign
	signedVal = fval;
	if (fval < 0) {
		fval = -fval;
	}

	// write the float number
	digits = 0;
	val = (int)fval;
	do {
		text[digits] = '0' + val % 10;
		digits++;
		val /= 10;
	} while (val);

	if (signedVal < 0) {
		text[digits] = '-';
		digits++;
	}

	buf = *buf_p;

	// fix precisiion
	if (prec < 0) {
		prec = 6;
	}

	if (prec) {
		width -= prec + 1;
	}
	// end

	while (digits < width) {
		*buf = ' ';
		buf++;
		width--;
	}

	while (digits-- > 0) {
		*buf = text[digits];
		buf++;
	}

	*buf_p = buf;

	// write the fraction
	digits = 0;
	while (digits < prec) {
		fval -= (int)fval;
		fval *= 10.0;
		val = (int)fval;
		text[digits] = '0' + val % 10;
		digits++;
	}

	if (digits > 0) {
		buf = *buf_p;
		*buf = '.';
		buf++;
		for (prec = 0; prec < digits; prec++) {
			*buf = text[prec];
			buf++;
		}
		*buf_p = buf;
	}
}

static void AddString(char **buf_p, const char *string, int width, int prec) {
	int size;
	char *buf;

	buf = *buf_p;

	if (string == NULL) {
		string = "(null)";
		prec = -1;
	}

	if (prec >= 0) {
		for (size = 0; size < prec; size++) {
			if (string[size] == '\0') {
				break;
			}
		}
	} else {
		size = strlen(string);
	}

	width -= size;

	while (size--) {
		*buf = *string;
		buf++;
		string++;
	}

	while (width-- > 0) {
		*buf = ' ';
		buf++;
	}

	*buf_p = buf;
}

/*
ED_vsprintf

I'm not going to support a bunch of the more arcane stuff in here
just to keep it simpler.  For example, the '$' is not
currently supported.  I've tried to make it so that it will just
parse and ignore formats we don't support.

returns: number of char written without ending '\0'
*/
/// TODO: How is is better/different then Q_vsnprintf?
int ED_vsprintf(char *buffer, const char *fmt, va_list ap) {
	char *buf_p;
	char ch;
	int flags;
	int width;
	int prec;
	int n;

	buf_p = buffer;

	while (qtrue) {
		// run through the format string until we hit a '%' or '\0'
		for (/*ch = *fmt */; (ch = *fmt) != '\0' && ch != '%'; fmt++) {
			*buf_p = ch;
			buf_p++;
		}
		if (ch == '\0') {
			break;
		}

		// skip over the '%'
		fmt++;

		// reset formatting state
		flags = 0;
		width = 0;
		prec = -1;
	rflag:
		ch = *fmt;
		fmt++;
	reswitch:
		switch (ch) {
		//case ' ':
		case '-':
			flags |= LADJUST;
			goto rflag;
		case '.':
			if (*fmt == '*') {
				fmt++;
				n = va_arg(ap, int);
				prec = n < 0 ? -1 : n;
				goto rflag;
			} else {
				n = 0;
				while (is_digit((ch = *fmt++))) {
					n = 10 * n + (ch - '0');
				}
				prec = n < 0 ? -1 : n;
				goto reswitch;
			}
		case '0':
			flags |= ZEROPAD;
			goto rflag;
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
			n = 0;
			do {
				n = 10 * n + (ch - '0');
				ch = *fmt;
				fmt++;
			} while (is_digit(ch));
			width = n;
			goto reswitch;
		case '*':
			width = va_arg(ap, int);
			goto rflag;
		case 'c':
			//TODO: make sure it's correct
			// *buf_p = va_arg( ap, char ); buf_p++;
			*buf_p = va_arg(ap, int);
			buf_p++;
			break;
		case 'd':
		case 'i':
			AddInt(&buf_p, va_arg(ap, int), width, flags);
			break;
		case 'f':
			AddFloat(&buf_p, va_arg(ap, double), width, prec, flags & REDUCE);
			break;
		case 's':
			AddString(&buf_p, va_arg(ap, char *), width, prec);
			break;
		case '%':
			*buf_p = ch;
			buf_p++;
			break;
			// edawn extension:
		case 'R':
			flags |= REDUCE;
			goto rflag;
		default:
			//TODO: make sure it's correct
			// *buf_p = va_arg( ap, char ); buf_p++;
			*buf_p = va_arg(ap, int);
			buf_p++;
			break;
		} // switch ( ch )
	}     // while ( qtrue )

	*buf_p = '\0';
	return buf_p - buffer;
}

/*
============================================================================
*/
