/// @file g_syscalls.c
/// This file is only included when building a dll.
/// g_syscalls.asm is included instead when building a qvm

#include "g_local.h"

#ifdef Q3_VM
#error "Do not use in VM build"
#endif

static dllSyscall_t syscall = (dllSyscall_t)-1;

/**
This method is called by server system to set pointer of system call (trap_*) handler to SV_DllSyscall.
The callstack is of the call:
```
dllEntry (syscallptr=<SV_DllSyscall>) at code/game/g_syscalls.c
VM_LoadDll (name="qagame", entryPoint=<vmTable+64>, systemcalls=<SV_DllSyscall>) at code/qcommon/vm.c
VM_Create (index=VM_GAME, systemCalls=<SV_GameSystemCalls>, dllSyscalls=<SV_DllSyscall>, interpret=VMI_NATIVE) at code/qcommon/vm.c
SV_InitGameProgs () at code/server/sv_game.c
SV_SpawnServer (mapname="q3dm1", killBots=qfalse) at code/server/sv_init.c
SV_Map_f () at code/server/sv_ccmds.c
Cmd_ExecuteString (text="devmap q3dm1") at code/qcommon/cmd.c
Cbuf_Execute () at code/qcommon/cmd.c
Com_Frame (noDelay=qfalse) at code/qcommon/common.c
main (argc, argv) at code/unix/unix_main.c
```
*/
DLLEXPORT void dllEntry(dllSyscall_t syscallptr) {
	syscall = syscallptr;
}

/// Print message on the local console.
void trap_Print(const char *text) {
	syscall(G_PRINT, text);
}

/// Abort the game with an error.
void trap_Error(const char *text) {
	syscall(G_ERROR, text);
}

/// Let the server system know where and how big the gentities
/// are, so it can look at them directly without going through an interface.
/// NOTE: gentity_t is actually sharedEntity_t
void trap_LocateGameData(gentity_t *gEnts, int numGEntities, int sizeofGEntity_t,
                         playerState_t *clients, int sizeofGClient) {
	syscall(G_LOCATE_GAME_DATA, gEnts, numGEntities, sizeofGEntity_t, clients, sizeofGClient);
}

/// Call before removing an interactive entity
void trap_UnlinkEntity(gentity_t *ent) {
	syscall(G_UNLINKENTITY, ent);
}

/// Userinfo strings are maintained by the server system, so they
/// are persistent across map loads, while all other game visible
/// data is completely reset.
void trap_GetUserinfo(int num, char *buffer, int bufferSize) {
	syscall(G_GET_USERINFO, num, buffer, bufferSize);
}

/// Write buffer to a file
void trap_FS_Write(const void *buffer, int len, fileHandle_t f) {
	syscall(G_FS_WRITE, buffer, len, f);
}

