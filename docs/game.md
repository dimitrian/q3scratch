# Server-Side QVM

## Engine Start
Before the game (modification) even loads, Quake 3 on startup:

1. Searches for/in pak (.pk3) files according to the server cvar `fs_game`.
2. Runs startup configuration (.cfg) files `default.cfg` and `q3config.cfg`.
3. Initializes the dynamic memory hunk (`com_hunkMegs`, `com_soundMegs`, `com_zoneMegs`).
4. Opens network sockets for listening.


## Game Start
The most straight-forward way to launch the game is to start a valid map, with
the server command map mapname. 

```
./quake3e.x64 +set sv_pure 0 +set vm_game 0 +set vm_cgame 0 +set vm_ui 1 +devmap q3dm1
```

Quake 3 on game start:
1. Loads the map/level (.bsp) file.
2. Looks through the pak (.pk3) files found on startup.
3. Loads the game qvm file (qagame.qvm).
4. Compiles the QVM bytecode to native code if `vm_game` is set to 2.
5. Jumps to the QVM procedure linked at code address 0 (i.e. [vmMain](@ref _g_vm_main))
6. Upon returning from the QVM procedure, does some other things (such as network tasks, reading in server commands, complaining with Hitch Warnings, etc.)
7. Jumps to code address 0 again 
8. Rinse, lather, repeat.

## vmMain
The QVM file lacks a symbol table, so the QVM interpreter/compiler subsystem
must make a blind decision on a reasonable entry point into the QVM. Currently,
Q3 simply branches to QVM code address 0. While Q3 (the engine) decides to jump
to code address 0, the game (mod) expects Q3A to jump into the function named
`vmMain`. To ensure these two expectations coincide, `vmMain` must be the first
function compiled before any other function into QVM bytecode. This means the
function `vmMain` must be the first fully-defined function in the first
assembled file. In baseq3, this is `vmMain` in `g_main.c`, which gets assembled
first as specified in the file game.q3asm, thus ensuring `vmMain` links as code
address 0. Note that other variables may be declared/defined before `vmMain`,
and other functions may be prototyped/declared before `vmMain`, but the first
function body to be compiled must be `vmMain`.

The function at code address 0 is the critical function. The associated name
can be anything, but for consistency I shall refer to code address 0 as `vmMain`,
as it is in `baseq3`.

On each server frame (time period determined by `cvar sv_fps`), the Q3 engine
calls `vmMain` with a number of arguments. The first argument (command in
`baseq3`) determines what major action to which the game should react. These
actions are enumerated in `g_public.h`, as enum type ::gameExport_t. Since the
arguments to `vmMain` are specified by the Q3 engine, and merely reflected in
::gameExport_t, modifying ::gameExport_t does not change anything in the game
module, except possibly to confuse it badly.
