# Main Page {#mainpage}

## QVM

In Q3 a virtual machine is called a QVM.
Three of them are loaded:
+ Server Side:
    * [game](game.md) : Always receive message: Perform gamelogic and hit bot.lib to perform A.I .
+ Client Side: Two virtual machine are loaded. Message are sent to one or the other depending on the gamestate:
    * [cgame](cgame.md) : Receive messages during Battle phases. Performs entity culling, predictions and trigger renderer.lib.
    * `q3_ui` : Receive messages during Menu phases. Uses system calls to draw the menus.
